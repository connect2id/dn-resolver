package com.nimbusds.ldap.dnresolver;


import com.unboundid.ldap.sdk.*;

import junit.framework.*;


/**
 * Tests user DN resolution based on a template.
 */
public class TemplateResolverTest extends TestCase {
	

	public void testGoodTemplate() {
	
		String template = "uid=%u,ou=people,dc=example,dc=com";
		
		assertTrue(TemplateResolver.isValidTemplate(template));
	}


	public void testBadTemplate1() {
	
		String template = "uid=jbrown,ou=people,dc=example,dc=com";
		
		assertFalse(TemplateResolver.isValidTemplate(template));
	}
	
	
	public void testBadTemplate2() {
	
		String template = "%u,ou=people,dc=example,dc=com";
		
		assertFalse(TemplateResolver.isValidTemplate(template));
	}
	
	
	public void testBadTemplate3() {
	
		String template = "%u";
		
		assertFalse(TemplateResolver.isValidTemplate(template));
	}
	
	
	public void testGoodResolve() {
	
		String template = "uid=%u,ou=people,dc=example,dc=com";
		
		DNResolver resolver = new TemplateResolver(template);
		
		String username = "jbrown";
		
		DN dn = null;
		
		try {
			dn = resolver.resolve(username);
			
		} catch (DNResolveException e) {
			
			fail(e.getMessage());
		}
		
		assertEquals("uid=jbrown,ou=people,dc=example,dc=com", dn.toString());
	}
}
