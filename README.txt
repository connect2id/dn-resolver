DN Resolver

Copyright (c) Connect2id Ltd., 2009 - 2014


README

This Java library provides two routines for resolving the distinguished name
(DN) of an LDAP directory object from a given entry attribute value:

* TemplateResolver: Resolution by simple template substitution.

* SearchResolver: Resolution through LDAP search operation.

The library is intended to resolve user DNs in applications that require LDAP
authentication of users. The DN is typically resolved from a unique user
attribute such as email address, name or ID.


Requirements:

* Java 1.6+
	
* UnboundID LDAP SDK 2.3.6.
	
* LDAP-v3 compatible directory


[EOF]