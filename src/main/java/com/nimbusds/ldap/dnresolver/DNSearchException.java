package com.nimbusds.ldap.dnresolver;


/**
 * Thrown to indicate an LDAP search exception to a {@link DNSearchRequest}.
 */
public class DNSearchException extends Exception {


	/**
	 * Creates a new DN search exception with the specified message.
	 *
	 * @param message The exception message.
	 */
	public DNSearchException(final String message) {
		
		this(message, null);
	}
	
	
	/**
	 * Creates a new DN search exception with the specified message and
	 * cause.
	 * 
	 * @param message The exception message.
	 * @param cause   The exception cause.
	 */
	public DNSearchException(final String message, final Throwable cause) {
	
		super("Couldn't do LDAP search: " + message, cause);
	}
}
