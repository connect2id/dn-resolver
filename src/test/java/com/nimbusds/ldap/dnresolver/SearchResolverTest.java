package com.nimbusds.ldap.dnresolver;


import java.util.*;

import com.unboundid.ldap.sdk.*;

import junit.framework.*;


/**
 * Tests user DN resolution based on LDAP search.
 */
public class SearchResolverTest extends TestCase {
	

	public void testGoodFilter() {
	
		String filter = "(uid=%u)";
		
		assertTrue(SearchResolver.isValidFilter(filter));
	}


	public void testBadFilter() {
	
		String filter = "boo=too";
		
		assertFalse(SearchResolver.isValidFilter(filter));
	}
	
	
	public void testGoodResolve() {
	
		DNSearchRequestHandler searchHandler = new SearchHandler();
		
		Properties props = System.getProperties();
		
		DN baseDN = null;
		
		try {
			baseDN = new DN(props.getProperty("test.ldap.search.baseDn"));
		
		} catch (Exception e) {

			fail(e.getMessage());
		}

		System.out.println("Property test.ldap.search.baseDn found");
		
		String scopeIn = props.getProperty("test.ldap.search.scope").toUpperCase();
		
		SearchScope scope = null;
		
		if (scopeIn.equals("ONE"))
			scope = SearchScope.ONE;
		else if (scopeIn.equals("SUB"))
			scope = SearchScope.SUB;
		else
			fail("Unexpected search scope property: " + scopeIn);
		
		String searchTemplate = props.getProperty("test.ldap.search.filter");
		
		DNResolver resolver = new SearchResolver(searchHandler, baseDN, scope, searchTemplate);
		
		String user = props.getProperty("test.searchResolver.goodInput");
		
		DN dn = null;
		
		try {
			dn = resolver.resolve(user);
			
		} catch (DNResolveException e) {
		
			fail("Failed to resolve user: " + user + ": " + e.getMessage());
		}
		
		assertNotNull(dn);
		
		System.out.println("User " + user + " successfully resolved to DN " + dn);
	}
	
	
	public void testBadResolve() {
	
		DNSearchRequestHandler searchHandler = new SearchHandler();
		
		Properties props = System.getProperties();
		
		DN baseDN = null;
		
		try {
			baseDN = new DN(props.getProperty("test.ldap.search.baseDn"));
		
		} catch (LDAPException e) {
			fail(e.getMessage());
		}
		
		String scopeIn = props.getProperty("test.ldap.search.scope").toUpperCase();
		
		SearchScope scope = null;
		
		if (scopeIn.equals("ONE"))
			scope = SearchScope.ONE;
		else if (scopeIn.equals("SUB"))
			scope = SearchScope.SUB;
		else
			fail("Unexpected search scope property: " + scopeIn);
		
		String searchTemplate = props.getProperty("test.ldap.search.filter");
		
		DNResolver resolver = new SearchResolver(searchHandler, baseDN, scope, searchTemplate);
		
		String user = props.getProperty("test.searchResolver.badInput");
		
		DN dn = null;
		
		try {
			dn = resolver.resolve(user);
			
		} catch (DNResolveException e) {
		
			fail("Failed to resolve user: " + user + ": " + e.getMessage());
		}
		
		assertNull(dn);
		
		System.out.println("User " + user + " successfully identified as non-resolvable");
	}
}
