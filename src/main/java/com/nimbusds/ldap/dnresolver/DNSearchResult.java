package com.nimbusds.ldap.dnresolver;


import com.unboundid.ldap.sdk.DN;


/**
 * Represents an LDAP search result returned to a {@link SearchResolver}. No 
 * attributes are included, just a list of the matching DNs.
 */
public class DNSearchResult {

	
	/**
	 * The matching DNs.
	 */
	private DN[] matches;
	

	/**
	 * Creates a new LDAP search result.
	 *
	 * @param matches The matching DNs, empty array or {@code null} if none
	 *                were found.
	 */
	public DNSearchResult(final DN[] matches) {
	
		if (matches == null) {
			this.matches = new DN[]{};
		} else {
			this.matches = matches;
		}
	}
	
	
	/**
	 * Creates a new LDAP search result with no matches.
	 */
	public DNSearchResult() {
	
		this(null);
	}
	
	
	/**
	 * Gets the matching DNs.
	 * 
	 * @return The matching DNs, empty array if none were found.
	 */
	public DN[] getMatches() {
	
		return matches;
	}
	
	
	/**
	 * Sets the matching DNs.
	 *
	 * @param matches The matching DNs, empty array if none were found.
	 *                Must not be {@code null}.
	 */
	public void setMatches(final DN[] matches) {
	
		if (matches == null)
			throw new IllegalArgumentException("The DN matches must not be null");
	
		this.matches = matches;
	}
}
