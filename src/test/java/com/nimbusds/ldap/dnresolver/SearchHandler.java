package com.nimbusds.ldap.dnresolver;


import java.util.*;

import com.unboundid.ldap.sdk.*;


/**
 * Test handler for LDAP DN search requests.
 */
public class SearchHandler implements DNSearchRequestHandler {

	
	private String ldapHost;
	
	private int ldapPort;
	
	private String bindDN;
	
	private String bindPassword;


	public SearchHandler() {
	
		Properties props = System.getProperties();
		
		ldapHost = props.getProperty("test.ldap.host");
		
		ldapPort = Integer.parseInt(props.getProperty("test.ldap.port"));
		
		bindDN = props.getProperty("test.ldap.user.dn");
		
		bindPassword = props.getProperty("test.ldap.user.password");
		
		System.out.println("Created new search handler for " + ldapHost + ":" + ldapPort);
	}
	
	
	public DNSearchResult search(final DNSearchRequest request)
		throws DNSearchException {
		
		LDAPConnection con;
		
		try {
			con = new LDAPConnection(ldapHost, ldapPort, bindDN, bindPassword);
			
		} catch (LDAPException e) {
		
			throw new DNSearchException("Couldn't connect to LDAP host: " + e.getMessage(), e);
		}
		
		SearchResult sr = null;
		
		try {
			sr = con.search(request.getBaseDN().toString(), request.getScope(), request.getFilter());
			
		} catch (LDAPException e) {
		
			throw new DNSearchException("Search request failed: " + e.getMessage(), e);
			
		} finally {
		
			con.close();
		}
		
		DN[] matches = new DN[sr.getEntryCount()];
		
		Iterator<SearchResultEntry> it = sr.getSearchEntries().iterator();
		
		for (int i=0; i < sr.getEntryCount(); i++) {
		
			try {
				matches[i] = it.next().getParsedDN();
				
			} catch (LDAPException e) {
				
				throw new DNSearchException("Bad entry DN: " + e.getMessage(), e);
			}
		}		
		
		return new DNSearchResult(matches);
	}

}
