package com.nimbusds.ldap.dnresolver;


import com.unboundid.ldap.sdk.DN;


/**
 * Interface for resolving a directory object's DN based on a unique attribute
 * value.
 */
public interface DNResolver {


	/**
	 * Resolves a directory object's distinguished name (DN).
	 *
	 * @param attributeValue The resolving attribute value which must be
	 *                       unique among all resolved entries. Examples: 
	 *                       username, user ID, email address.
	 *
	 * @return The resolved DN, {@code null} if no matching DN could be 
	 *         found.
	 *
	 * @throws IllegalArgumentException If the input attribute is 
	 *                                  {@code null} or empty string.
	 * @throws DNResolveException       If DN resolution failed.
	 */
	public DN resolve(final String attributeValue) throws DNResolveException;
}
