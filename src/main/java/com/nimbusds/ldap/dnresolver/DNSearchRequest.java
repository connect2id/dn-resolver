package com.nimbusds.ldap.dnresolver;


import com.unboundid.ldap.sdk.*;


/**
 * Represents an LDAP search request from a {@link SearchResolver}. No 
 * attributes are required, the request is only interested in finding a
 * matching DN.
 */
public class DNSearchRequest {

	/**
	 * The base DN for the search operation.
	 */
	private DN baseDN;
	
	
	/**
	 * The search scope.
	 */
	private SearchScope scope;
	
	
	/**
	 * The search filter.
	 */
	private Filter filter;
	

	/**
	 * Creates a new LDAP search request.
	 *
	 * @param baseDN The base DN for the search operation. Must not be
	 *               {@code null}.
	 * @param scope  The search scope. Must not be {@code null}.
	 * @param filter The search filter. Must not be {@code null}.
	 */
	public DNSearchRequest(final DN baseDN, final SearchScope scope, final Filter filter) {
	
		if (baseDN == null || scope == null || filter == null)
			throw new NullPointerException("Null arguments not allowed");
	
		this.baseDN = baseDN;
		this.scope = scope;
		this.filter = filter;
	}
	
	
	/**
	 * Gets the base DN for the search operation.
	 * 
	 * @return The base DN.
	 */
	public DN getBaseDN() {
	
		return baseDN;
	}
	
	
	/**
	 * Gets the search scope.
	 *
	 * @return The search scope.
	 */
	public SearchScope getScope() {
	
		return scope;
	}
	
	
	/**
	 * Gets the search filter.
	 *
	 * @return The search filter.
	 */
	public Filter getFilter() {
	
		return filter;
	}
}
