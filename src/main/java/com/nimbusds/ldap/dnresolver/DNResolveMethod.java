package com.nimbusds.ldap.dnresolver;


/**
 * Enumerates the methods for resolving a user's DN.
 */
public enum DNResolveMethod {

	
	/**
	 * Resolve by substituting the username in a DN template.
	 */
	TEMPLATE,
	
	
	/**
	 * Resolve by performing a search.
	 */
	SEARCH,
	
	
	/**
	 * No resolve is required. May be used in situations when a SASL bind
	 * with a username is available and no user DN resolution is required.
	 */
	NONE
}
