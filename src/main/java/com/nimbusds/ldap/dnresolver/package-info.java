/**
 * Provides two routines for resolving the distinguished name (DN) of an LDAP
 * directory object from a given entry attribute value:
 *
 * <ul>
 *     <li>{@link com.nimbusds.ldap.dnresolver.TemplateResolver}
 *         Resolution by simple template substitution.
 *     <li>{@link com.nimbusds.ldap.dnresolver.SearchResolver} 
 *         Resolution through LDAP search operation.
 * </ul>
 */
package com.nimbusds.ldap.dnresolver;
