package com.nimbusds.ldap.dnresolver;


import com.unboundid.ldap.sdk.*;


/**
 * Resolves a directory object's DN by performing an LDAP search request.
 *
 * <p>Example: Resolving a user's DN based on their username or email address:
 *
 * <ul>
 *     <li>Input is "jbrown@example.com"
 *     <li>The search criteria:
 *         <ul>
 *             <li>Base DN: "ou=people,dc=example,dc=com"
 *             <li>Scope: ONE
 *             <li>Search filter template: "(|(uid=%u)(mail=%u))"
 *         </ul>
 *     <li>The resolved DN is "uid=jbrown,ou=people,dc=example,dc=com"
 * </ul>
 */
public class SearchResolver implements DNResolver {


	/** 
	 * The handler of LDAP search requests. 
	 */
	DNSearchRequestHandler searchHandler;
	

	/** 
	 * The search base DN. 
	 */
	final DN searchBaseDN;
	
	
	/** 
	 * The search scope. 
	 */
	final SearchScope searchScope;
	
	
	/** 
	 * The search filter template. 
	 */
	final String searchFilter;
	

	/**
	 * Creates a new DN resolver that uses an LDAP search operation.
	 *
	 * @param searchHandler The handler of LDAP search requests.
	 * @param searchBaseDN  The search base DN.
	 * @param searchScope   The search scope.
	 * @param searchFilter  The search filter template, must contain a 
	 *                      {@code %u} placeholder.
	 *
	 * @throws IllegalArgumentException If the search filter template is
	 *                                  invalid.
	 */
	public SearchResolver(final DNSearchRequestHandler searchHandler, 
	                      final DN searchBaseDN, 
			      final SearchScope searchScope,
			      final String searchFilter) {
		
		if (searchHandler == null)
			throw new NullPointerException("The search handler argument must not be null");
			
		if (searchBaseDN == null)
			throw new NullPointerException("DN search base argument must not be null");
		
		if (searchScope == null)
			throw new NullPointerException("SearchScope argument must not be null");
	
		if (searchFilter == null)
			throw new NullPointerException("Search string filter argument must not be null");
	
		if (! isValidFilter(searchFilter))
			throw new IllegalArgumentException("Bad search filter template");
	
		this.searchHandler = searchHandler;
		this.searchBaseDN = searchBaseDN;
		this.searchScope = searchScope;
		this.searchFilter = searchFilter;
	}
	
	
	/**
	 * Returns {@code false} if the specified search template doesn't
	 * contain at least one {@code %u} placeholder or the final string 
	 * doesn't represent a valid LDAP search filter.
	 *
	 * @param searchFilter The search filter, must contain an {@code %u} 
	 *                     placeholder.
	 *
	 * @return {@code true} if the the filter is valid, else {@code false}.
	 */
	public static boolean isValidFilter(final String searchFilter) {
	
		if (searchFilter == null)
			return false;
		
		if (searchFilter.indexOf("%u") == -1)
			return false;
		
		final String testUsername = "user001";
		
		final String searchFilterComplete = searchFilter.replaceAll("%u", testUsername);
		
		try {
			Filter.create(searchFilterComplete);
			
		} catch (LDAPException e) {
			return false;
		}
		
		return true;
	}


	@Override
	public DN resolve(final String attribute) 
		throws DNResolveException {
		
		if (attribute == null || attribute.isEmpty())
			throw new IllegalArgumentException("Bad attribute value: must not be null or empty string");
		
		// Escape special chars
		final String sanitizedAttribute = Filter.encodeValue(attribute);
		
		
		// Create search filter from template
		
		Filter filter = null;
		
		try {
			filter = Filter.create(searchFilter.replaceAll("%u", sanitizedAttribute));
		
		} catch (LDAPException e) {
		
			throw new DNResolveException("Bad attribute value: Resulting search filter is invalid");
		}
		
		
		// Run LDAP search
		
		DNSearchResult result = null;
		
		try {
			result = searchHandler.search(new DNSearchRequest(searchBaseDN, searchScope, filter));
			
		} catch (DNSearchException e) {
			throw new DNResolveException(e.getMessage(), e.getCause());
		}
		
		DN[] matches = result.getMatches();
			
		if (matches.length == 0)
			return null; // no match found
			
		else if (matches.length > 1)
			throw new DNResolveException("More than one matching DN found");
			
		return matches[0];
	}
}
