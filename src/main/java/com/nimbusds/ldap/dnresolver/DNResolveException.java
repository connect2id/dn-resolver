package com.nimbusds.ldap.dnresolver;


/**
 * Thrown to indicate a DN resolve exception.
 */
public class DNResolveException extends Exception {


	/**
	 * Creates a new DN resolve exception with the specified message.
	 *
	 * @param message The exception message.
	 */
	public DNResolveException(final String message) {
		
		this(message, null);
	}
	
	
	/**
	 * Creates a new DN resolve exception with the specified message and
	 * cause.
	 * 
	 * @param message The exception message.
	 * @param cause   The exception cause.
	 */
	public DNResolveException(final String message, final Throwable cause) {
	
		super("Couldn't resolve DN: " + message, cause);
	}
}
