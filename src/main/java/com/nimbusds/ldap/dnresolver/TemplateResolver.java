package com.nimbusds.ldap.dnresolver;


import com.unboundid.ldap.sdk.*;


/**
 * Resolves a directory object's DN using template substitution. 
 *
 * <p>This class is specifically intended to resolve a user's DN based on a
 * unique fragment contained in its RDN, such as an UID, name or email address. 
 * Every occurence of a "%u" placeholder in the template is replaced by the
 * provided input string.
 *
 * <p>Example: Resolving a user's DN based on their username (which is used as 
 * the value part of the RDN):
 *
 * <ul>
 *     <li>Input is "jbrown"
 *     <li>DN template is "uid=%u,ou=people,dc=example,dc=com"
 *     <li>The resolved DN is "uid=jbrown,ou=people,dc=example,dc=com"
 * </ul>
 *
 * <p>This is the simplest and most efficient method to resolve a user's DN. 
 * It doesn't require any LDAP requests (search) to be sent to the back-end 
 * directory. Note, however, that the actual existence of the DN is not 
 * verified!
 */
public class TemplateResolver implements DNResolver {


	/** 
	 * The DN template. 
	 */
	final String template;
	

	/**
	 * Creates a new DN resolver based on a DN template.
	 *
	 * @param template The DN template, must contain a "%u" placeholder.
	 *
	 * @throws IllegalArgumentException If the DN template is invalid.
	 */
	public TemplateResolver(final String template) {
	
		if (! isValidTemplate(template))
			throw new IllegalArgumentException("Bad DN template");
	
		this.template = template;
	}
	
	
	/**
	 * Returns {@code false} if the specified template doesn't contain at 
	 * least one "%u" placeholder or the final string doesn't represent a 
	 * valid DN.
	 *
	 * @param template The DN template string to validate.
	 *
	 * @return {@code true} if the the template is valid, else {@code false}.
	 */
	public static boolean isValidTemplate(final String template) {
	
		if (template == null)
			return false;
		
		if (template.indexOf("%u") == -1)
			return false;
		
		final String testUsername = "user001";
		
		final String dnString = template.replaceAll("%u", testUsername);
		
		try {
			new DN(dnString);
			
		} catch (LDAPException e) {
			return false;
		}
		
		return true;
	}


	@Override
	public DN resolve(final String attribute) 
		throws DNResolveException {
		
		if (attribute == null || attribute.isEmpty())
			throw new IllegalArgumentException("Bad attribute value: must not be null or empty string");
		
		final String dnString = template.replaceAll("%u", attribute);
		
		try {
			return new DN(dnString);
		
		} catch (LDAPException e) {
		
			throw new DNResolveException("The syntax of the resolved DN is invalid");
		}	
	}
}
