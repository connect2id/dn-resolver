package com.nimbusds.ldap.dnresolver;


import com.unboundid.ldap.sdk.*;

import junit.framework.*;


/**
 * Tests the LDAP search filter sanitizer method of the
 * com.unboundid.ldap.sdk.Filter class.
 */
public class SearchFilterSanitizerTest extends TestCase {
	

	public void test1() {
	
		String v = "abc";
		
		String sv = Filter.encodeValue(v);
		
		System.out.println("Sanitize: " + v + " -> " + sv);
		
		assertEquals("abc", sv);
	}
	
	
	public void test2() {
	
		String v = "abc*def";
		
		String sv = Filter.encodeValue(v);
		
		System.out.println("Sanitize: " + v + " -> " + sv);
		
		assertEquals("abc\\2adef", sv);
	}
	
	
	public void test3() {
	
		String v = "(abc)";
		
		String sv = Filter.encodeValue(v);
		
		System.out.println("Sanitize: " + v + " -> " + sv);
		
		assertEquals("\\28abc\\29", sv);
	}
	
	
	public void test4() {
	
		String v = "\u0000abc";
		
		String sv = Filter.encodeValue(v);
		
		System.out.println("Sanitize: " + v + " -> " + sv);
		
		assertEquals("\\00abc", sv);
	}
	
	
	public void test5() {
		// Euro sign
		String v = "\u20ac";
		
		String sv = Filter.encodeValue(v);
		
		System.out.println("Sanitize: " + v + " -> " + sv);
		
		assertEquals("\\e2\\82\\ac", sv);
	}
	
	
	public void test6() {
	
		String v = "\u010d";
		
		String sv = Filter.encodeValue(v);
		
		System.out.println("Sanitize: " + v + " -> " + sv);
		
		assertEquals("\\c4\\8d", sv);
	}
}
