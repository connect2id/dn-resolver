package com.nimbusds.ldap.dnresolver;


/**
 * Interface for handling LDAP search requests from a {@link SearchResolver}.
 */
public interface DNSearchRequestHandler {


	/**
	 * Handles an LDAP search request from a {@link SearchResolver}.
	 *
	 * @param searchRequest The search request.
	 *
	 * @return The search result.
	 *
	 * @throws DNSearchException If the search request failed.
	 */
	public DNSearchResult search(final DNSearchRequest searchRequest) throws DNSearchException;
}
